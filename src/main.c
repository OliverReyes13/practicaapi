#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "lista.h"
#include "lista.c"


int main(int argc, char const *argv[]){
    
    printf("Numeros Enteros \n");
    Lista1 *A;
    A = lista_nueva1();
    lista_agrega1(A, 99);
    lista_agrega1(A, 12);
    lista_agrega1(A, 32);
    lista_agrega1(A, -987);
    lista_muestra1(A);
    lista_libera1(A);

    printf("Numeros Flotantes \n");
    Lista *L;
    L = lista_nueva();
    lista_agrega(L, 09.2234);
    lista_agrega(L, 56.921);
    lista_agrega(L, 14.493);
    lista_agrega(L, -97.17);
    lista_muestra(L);
    lista_libera(L);
    


    return 0;
    
}
